package com.infosys.iisonline.service.route;

import java.util.Arrays;
import java.util.List;
import javax.sql.DataSource;
import org.apache.camel.component.servlet.CamelHttpTransportServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.context.annotation.ComponentScan;

/**
 * This the SpringBoot setup, we currently register a couple of Beans on the fly
 * instead of having a test only spring.xml file
 * 
 * @author Diego_Bragato
 *
 */
@SpringBootApplication
@ComponentScan("com.infosys.iisonline.service")
@EnableAutoConfiguration
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	/**
	 * This bean behave like the servlet configuration in web.xml.
	 * Here the servlet name is set explicitly to have consistency between test and main
	 * This is tricky cause the default name of the CamelServlet in spring Boot is httpTransportCamelServlet
	 * where the default name in Tomcat is CamelServlet
	 * @return
	 */
	@Bean
	public ServletRegistrationBean servletRegistrationBean() {
		ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(
				new CamelHttpTransportServlet(), "/services/*");
		servletRegistrationBean.setName("CamelServlet");
		return servletRegistrationBean;
	}

	/**
	 * This datasource bean is configured for in memory automated test purposes.
	 * To use hsqldb standalone server in stead of the in memory you may use:
	 * dataSource.setUrl("jdbc:hsqldb:hsql://localhost/sandboxDb");
	 * 
	 * @return
	 */
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.hsqldb.jdbc.JDBCDriver");
		// this is hsqldb in memory
		dataSource.setUrl("jdbc:hsqldb:mem:sandboxDb");
		dataSource.setUsername("SA");
		dataSource.setPassword("");
		return dataSource;
	}

	/**
	 * This Bean is used to simulate the back-end database.
	 * @return
	 */
	@Bean
	public List<String> serviceUrls() {
		return Arrays.asList("http://localhost:8888/index.html",
				"http://localhost:8889/index.html",
				"http://localhost:8890/index.html");
	}
}
