package com.infosys.iisonline.service.route;

import org.junit.Test;
import org.junit.runner.RunWith;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import static org.junit.Assert.assertEquals;
import org.springframework.web.client.RestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.springframework.boot.test.SpringApplicationConfiguration;

/**
 * @author Diego_Bragato
 * This is a major integration Test Class covering 
 * all relevant REST services of the application
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest("server.port=8888")
public class RouterTest {
	private RestTemplate restTemplate = new RestTemplate();
	/**
	 * This is a health check just to show that the automated 
	 * test integration test setup is fine 
	 * @throws IOException
	 */
	@Test
	public void testHealthPage() throws IOException {
		String page = restTemplate.getForObject(
				"http://localhost:8888/index.html", String.class);
		String compare = FileUtils.readFileToString(new File(
				"src/main/webapp/index.html"));
		assertEquals("Compare html:", compare, page);
	}
}
