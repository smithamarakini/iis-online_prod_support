package com.infosys.iisonline.service.route;

import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.http.client.ClientHttpResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;

public class CustomErrorHandler implements ResponseErrorHandler {
    public boolean hasError(ClientHttpResponse response)
	throws IOException
    {
	// ???
	return true;
    }
    public void handleError(ClientHttpResponse response)
	throws IOException    {
	InputStream is = response.getBody();
	BufferedReader in = new BufferedReader(new InputStreamReader(is));
	System.out.println("Custom Error Handler:");
	while(in.ready()){
	    System.out.println (in.readLine());
	}
    }
}
