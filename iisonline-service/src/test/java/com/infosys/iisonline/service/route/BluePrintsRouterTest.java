package com.infosys.iisonline.service.route;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest("server.port=8888")
public class BluePrintsRouterTest {
	private RestTemplate restTemplate = new RestTemplate();

	@Test
	public void testStaticFileGet() throws IOException {
		// TODO make error handler working
		// restTemplate.setErrorHandler(new CustomErrorHandler());
		String page = restTemplate.getForObject(
				"http://localhost:8888/services/bluePrintsStaticFile",
				String.class);
		String compare = FileUtils
				.readFileToString(new File(
						"src/main/resources/com/infosys/iisonline/service/route/SearchResponse.json"));
		assertEquals("Compare json:", compare, page);

	}

	@Test
	public void testSqlServicePost() throws IOException {
		String page = restTemplate.postForObject(
				"http://localhost:8888/services/bluePrintsSql", null,
				String.class);
		// TODO this is hard coded test result
		// and it shall be from DB without camel
		assertEquals("Compare Sql Post:", "http://provider3:8003//api", page);
	}

	// @Test
	public void testOrmService() {
		// TODO this will fail
		String page = restTemplate.getForObject(
				"http://localhost:8888/services/bluePrintsOrm", String.class);
		assertEquals("Compare Orm Get:", null, page);
		fail();
	}

	@Test
	public void testSpringBeanService() {
		String page = restTemplate.getForObject(
				"http://localhost:8888/services/bluePrintsSpringBean", String.class);
		// TODO this is hard coded
		// the output of the test shall be compared to data from spring bean directly
		assertEquals("Compare Spring Bean Get:", "http://localhost:8890/index.html", page);
	}
}
