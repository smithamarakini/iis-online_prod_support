package com.infosys.iisonline.service;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/health")
public class HealthCheckController {
    @RequestMapping(method=RequestMethod.GET)
    public String check() {
	// TODO do something better for health checks
	return "../services/hello?name=HealthyBoy";
    }
}
