package com.infosys.iisonline.service.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 * This is the main router of the application,
 * to expose Rest services and do integrations
 * @author Diego_Bragato
 */
@Component
public class Router extends RouteBuilder {

	/**
	 *	Setup Camel servlet and expose services
	 */
	@Override
	public void configure() throws Exception {

		restConfiguration().component("servlet")
				.componentProperty("servletName", "CamelServlet")
				.endpointProperty("servletName", "CamelServlet");
		
		// TODO implementation
	}

}
