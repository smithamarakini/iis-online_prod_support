package com.infosys.iisonline.service.camel;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class ListAggregationStrategy implements AggregationStrategy {
	@SuppressWarnings("unchecked")
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
		Exchange aggregatedExchange = null;
		if (oldExchange != null) {
			List<?> newBodyItems = newExchange.getIn().getBody(List.class);
			oldExchange.getIn().getBody(List.class).addAll(newBodyItems);
			aggregatedExchange = oldExchange;
		}else{
			aggregatedExchange = newExchange;
		}
		return aggregatedExchange;
	}
}
