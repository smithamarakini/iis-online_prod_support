package com.infosys.iisonline.service.route;

import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.processor.aggregate.UseLatestAggregationStrategy;
import org.springframework.stereotype.Component;

import com.infosys.iisonline.service.camel.DataSetToUrlTransformer;
/**
 * 
 * @author Diego_Bragato
 * This class show Example usage of Apache camel for various integration patterns.
 */
@Component
public class BluePrintsRouter extends RouteBuilder {
	private Processor dataSetToUrlTransformer = new DataSetToUrlTransformer();

	/**
	 * Overall Services Configuration
	 */
	@Override
	public void configure() throws Exception {
		restConfiguration().component("servlet")
				.componentProperty("servletName", "CamelServlet")
				.endpointProperty("servletName", "CamelServlet");
		configureStaticFileService();
		configureSqlService();
		configureSpringBeanService();
		configureOrmService();
	}

	/**
	 * Example REST service to show how to fetch data from ORM and Database
	 */
	private void configureOrmService() {
		// TODO Auto-generated method stub
	}

	/**
	 * Example REST service to show how to fetch data from a Spring Bean
	 */
	private void configureSpringBeanService() {
		rest("/bluePrintsSpringBean").get().to("direct:bluePrintsSpringBean:GET");

		// TODO reorder and check duplicates e.g. convertBodyTo String
		from("direct:bluePrintsSpringBean:GET").convertBodyTo(String.class)
				.setHeader("providerUrls").simple("ref:serviceUrls")
				.setHeader("sequenceId").simple("${id}").split()
				.header("providerUrls")
				.aggregationStrategy(new UseLatestAggregationStrategy())
				.convertBodyTo(String.class)
				.to("log:Provider?level=INFO&showAll=true&multiline=true")
				.end().setHeader("Content-Type")
				.constant("application/json;charset=UTF-8");
	}

	/**
	 * Example REST service to show how to fetch data from a file
	 */
	private void configureStaticFileService() {
		rest("/bluePrintsStaticFile").get().to("direct:bluePrintsStaticFile:GET");

		from("direct:bluePrintsStaticFile:GET")
				.setHeader("Content-Type")
				.constant("application/json;charset=UTF-8")
				.setBody()
				.simple("resource:classpath:/com/infosys/iisonline/service/route/SearchResponse.json")
				.convertBodyTo(String.class);
	}

	/**
	 * Example REST service to show how to fetch data from SQL and database exposing a POST
	 */
	private void configureSqlService() {
		rest("/bluePrintsSql").post().to("direct:bluePrintsSql:POST");

		from("direct:bluePrintsSql:POST")
				.convertBodyTo(String.class)
				.to("sql:select * from providers ?dataSourceRef=dataSource&outputHeader=providers")
				.to("log:Debug?level=INFO&showAll=true&multiline=true")
				.bean(dataSetToUrlTransformer).setHeader("sequenceId")
				.simple("${id}").split().header("providerUrls")
				.aggregationStrategy(new UseLatestAggregationStrategy())
				//.to("direct:requestToProvider")
				.convertBodyTo(String.class)
				//.to("log:Provider?level=INFO&showAll=true&multiline=true")
				.end()
				.to("log:Aggregated?level=INFO&showAll=true&multiline=true")
				.setHeader("Content-Type")
				.constant("application/json;charset=UTF-8");
	}
}
