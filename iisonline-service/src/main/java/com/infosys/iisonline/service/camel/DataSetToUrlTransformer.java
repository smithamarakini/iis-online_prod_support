package com.infosys.iisonline.service.camel;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class DataSetToUrlTransformer implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		@SuppressWarnings("unchecked")
		List<Map<String, Object>> providerData = exchange.getIn().getHeader(
				"providers", List.class);

		List<String> providerUrls = new ArrayList<String>();
		for (Map<String, Object> urlData : providerData) {
			providerUrls.add(String.format("http://%s:%s/%s",
					urlData.get("server_name"), urlData.get("port"),
					urlData.get("context_path")));
		}
		exchange.getIn().setHeader("providerUrls", providerUrls);
	}

}
