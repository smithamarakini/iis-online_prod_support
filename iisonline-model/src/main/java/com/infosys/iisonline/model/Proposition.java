package com.infosys.iisonline.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="PROPOSITION")
public class Proposition {
	@Id
	@GeneratedValue
	private Integer id;
	@Column(name="img_url",nullable = false)
	private String imgUrl;
	@Column(name="video_url",nullable = false)
	private String videoUrl;
	@Column(name="title",nullable = false)
	private String title;
	@ManyToOne
	@JoinColumn(name = "proposition_status_id")
	private PropositionStatus propositionStatus;
	@Column(name="description", nullable = false)
	private String description;
	@Column(name="score", nullable = false)
	private Float score;
	
	protected Proposition(){
	}
	public Proposition(Integer id, String imgUrl,
					   String videoUrl, String title, PropositionStatus propositionStatus,
					   String description, Float score) {
		super();
		this.id = id;
		this.imgUrl = imgUrl;
		this.videoUrl = videoUrl;
		this.title = title;
		this.propositionStatus = propositionStatus;
		this.description = description;
		this.score = score;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getVideoUrl() {
		return videoUrl;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public PropositionStatus getPropositionStatus() {
		return propositionStatus;
	}

	public void setPropositionStatus(PropositionStatus propositionStatus) {
		this.propositionStatus = propositionStatus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getScore() {
		return score;
	}

	public void setScore(Float score) {
		this.score = score;
	}	
}
