package com.infosys.iisonline.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
// TODO
// ideally avoid to use hibernate validators and use only javax ones
import org.hibernate.validator.constraints.Email;
// TODO
// this can be simulated with @Size(min=1) and @NotNull combined
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="USER")
public class User {
	@Id
	@GeneratedValue
	private Integer id;
	@Column(name="user_name", nullable = false)
	@NotEmpty(message = "Please enter the User Name")
	@Email
	@Pattern(regexp = ("^[A-Za-z0-9-_]+@[A-Za-z]+\\.+[a-zA-Z]{2,4}$"))
	private String userName;
	@Column(name="password",nullable = false)
	@NotEmpty(message = "Please enter the Password")
	private String password;
	@Column(name="enabled",nullable = false)
	private Boolean enabled;
	/**
	 * Required by JPA specs
	 */
	protected User(){
	}
	public User(String userName, String password, Boolean enabled) {
		this.userName = userName;
		this.password = password;
		this.enabled = enabled;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
}
