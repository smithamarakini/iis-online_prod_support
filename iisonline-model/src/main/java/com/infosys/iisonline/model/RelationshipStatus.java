package com.infosys.iisonline.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="RELATIONSHIP_STATUS")
public class RelationshipStatus {
	@Id
	@GeneratedValue
	private Integer id;
	@Column(name="relationship_status_id", nullable = false)
	private String relationshipStatusId;
	@Column(name="Rel_Status",nullable = false)
	private String relStatus;
	/**
	 * Required by JPA specs
	 */
	protected RelationshipStatus(){
	}
	public RelationshipStatus(String relationshipStatusId, String relStatus) {
		this.relationshipStatusId = relationshipStatusId;
		this.relStatus = relStatus;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRelationshipStatusId() {
		return relationshipStatusId;
	}
	public void setRelationshipStatusId(String relationshipStatusId) {
		this.relationshipStatusId = relationshipStatusId;
	}
	public String getRelStatus() {
		return relStatus;
	}
	public void setRelStatus(String relStatus) {
		this.relStatus = relStatus;
	}
	
}