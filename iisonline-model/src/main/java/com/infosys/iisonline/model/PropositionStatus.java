package com.infosys.iisonline.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PROPOSITION_STATUS")
public class PropositionStatus {
	@Id
	@GeneratedValue
	private Integer id;
	@Column(name="status",nullable = false)
	private String status;
	/**
	 * Required by JPA specs
	 */
	protected PropositionStatus(){
	}
	public PropositionStatus(String status) {
		this.status = status;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}	
}
