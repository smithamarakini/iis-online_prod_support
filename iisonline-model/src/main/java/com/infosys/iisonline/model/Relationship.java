package com.infosys.iisonline.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;


@Entity
@Table(name="RELATIONSHIP")
public class Relationship {

	@Id
	@GeneratedValue
	private Integer id;
	@ManyToOne
	@JoinColumn(name = "proposition_id")
	private Proposition proposition;
	@ManyToOne
	@JoinColumn(name = "relationship_status_id")
	private RelationshipStatus relationshipStatus;
	@ManyToOne
	@JoinColumn(name = "investor_profile_id")
	private InvestorProfile investorProfile;
	
	protected Relationship(){
	}
	
	public Relationship(Integer id, Proposition proposition,
			RelationshipStatus relationshipStatus, InvestorProfile investorProfile) {
		super();
		this.id = id;
		this.proposition = proposition;
		this.relationshipStatus = relationshipStatus;
		this.investorProfile = investorProfile;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Proposition getProposition() {
		return proposition;
	}

	public void setProposition(Proposition proposition) {
		this.proposition = proposition;
	}

	public RelationshipStatus getRelationshipStatus() {
		return relationshipStatus;
	}

	public void setRelationshipStatus(RelationshipStatus relationshipStatus) {
		this.relationshipStatus = relationshipStatus;
	}

	public InvestorProfile getInvestorProfile() {
		return investorProfile;
	}

	public void setInvestorProfile(InvestorProfile investorProfile) {
		this.investorProfile = investorProfile;
	}
	
	
}
