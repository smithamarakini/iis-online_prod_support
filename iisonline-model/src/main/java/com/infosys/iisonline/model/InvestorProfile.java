package com.infosys.iisonline.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

@Entity
@Table(name="INVESTOR_PROFILE")
public class InvestorProfile {
	@Id
	@GeneratedValue
	private Integer id;
	@Column(name="user_id", nullable = false)
	private String userId;
	@Column(name="first_name",nullable = false)
	private String firstName;
	@Column(name="second_name",nullable = false)
	private String secondName;
	@Column(name="street",nullable = false)
	private String street;
	@Column(name="zip_code", nullable = false)
	private String zipCode;
	@Column(name="city", nullable = false)
	private String city;

	@ManyToOne
	@JoinColumn(name = "region_id")
	private GeographicalRegion geographicalRegion;
	
	@Column(name="country", nullable = false)
	private String country;
	@Column(name="status", nullable = false)
	private Integer status;
	
	protected InvestorProfile(){
	}

	public InvestorProfile(Integer id, String userId, String firstName,
						   String secondName, String street, String zipCode, String city,
						   GeographicalRegion geographicalRegion, String country, Integer status) {
		super();
		this.id = id;
		this.userId = userId;
		this.firstName = firstName;
		this.secondName = secondName;
		this.street = street;
		this.zipCode = zipCode;
		this.city = city;
		this.geographicalRegion = geographicalRegion;
		this.country = country;
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public GeographicalRegion getGeographicalRegion() {
		return geographicalRegion;
	}

	public void setGeographicalRegion(GeographicalRegion geographicalRegion) {
		this.geographicalRegion = geographicalRegion;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
