package com.infosys.iisonline.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Column;

/**
 * This Entity is now here for blueprint purposes of the JPA stuff,
 * This entity shall be either refactored into "Proposition" and become 
 * a real database table or refactored into BLUEPRINT_STH table
 */
@Entity
@Table(name="PROVIDER")
public class Provider {
	@Id
	@GeneratedValue
	private Integer id;
	@Column(name="server_name", nullable = false)
	private String serverName;
	@Column(name="server_port",nullable = false)
	private String serverPort;
	@Column(name="context_path",nullable = false)
	private String contextPath;

	/**
	 * no-args constructor required by JPA spec
	 */
	protected Provider() {
	}
	public Provider(String serverName,String serverPort,String contextPath){
		this.serverName = serverName;
		this.serverPort = serverPort;
		this.contextPath = contextPath;
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getServerPort() {
		return serverPort;
	}

	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}

	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}
}
