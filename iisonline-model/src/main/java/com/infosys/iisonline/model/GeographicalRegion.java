package com.infosys.iisonline.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GEOGRAPHICAL_REGION")
public class GeographicalRegion {
	@Id
	@GeneratedValue
	private Integer id;
	@Column(name="name", nullable = false)
	private String name;
	/**
	 * Required by JPA specs
	 */
	protected GeographicalRegion(){
	}
	public GeographicalRegion(String name) {
		this.name = name;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
