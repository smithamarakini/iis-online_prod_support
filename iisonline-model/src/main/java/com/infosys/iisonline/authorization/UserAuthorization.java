package com.infosys.iisonline.authorization;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.infosys.iisonline.model.*;
import com.infosys.iisonline.model.Role;
import com.infosys.iisonline.model.User;
//import com.infosys.iisonline.model.User_;

public class UserAuthorization {

	private EntityManager manager = null;
	private EntityManagerFactory factory = null;
	//private AnnotationConfigApplicationContext ctx = null;

	public UserAuthorization() {
		factory = Persistence.createEntityManagerFactory("persistenceUnit");
		manager = factory.createEntityManager();
		// TODO
		// some mess here unclear if we need this here
		// can we have bean config in the spring xml ?
		//ctx = new AnnotationConfigApplicationContext();
		//ctx.register(ContextConfig.class);
		//ctx.refresh();
	}
	public void setEntityManagerFactory(EntityManagerFactory factory){
		this.factory = factory;
	}
	//public EntityManagerFactory getEntityManagerFactory(){
	//return factory;
	//}

	// public String checkUser(String userName) {

	// 	String userFound = null;
	// 	try {
	// 		boolean validUser = validateUser(userName);

	// 		if (validUser == true) {
	// 			Role role = getRoleFromName(userName);
	// 			System.out.println("Role name for the logged in user"
	// 					+ role.getRoleName());
	// 			userFound = role.getRoleName();
	// 		} else {
	// 			userFound = "Not a valid user";
	// 		}
	// 	} catch (NullPointerException e) {
	// 		e.getStackTrace();
	// 		throw e;
	// 	} catch (Exception e) {
	// 		e.getStackTrace();
	// 	}
	// 	return userFound;
	// }

	// private boolean validateUser(String userName) {
	// 	boolean userFound = false;

	// 	CriteriaBuilder cb = manager.getCriteriaBuilder();

	// 	try {
	// 		CriteriaQuery<User> listCriteria = cb.createQuery(User.class);
	// 		Root<User> listRoot = listCriteria.from(User.class);
	// 		listCriteria.select(listRoot);
	// 		listCriteria
	// 				.where(cb.equal(listRoot.get(User_.userName), userName));

	// 		TypedQuery<User> tQuery = manager.createQuery(listCriteria);
	// 		List<User> users = tQuery.getResultList();

	// 		if (users.size() == 1) {
	// 			userFound = true;
	// 			System.out.println("user found in the DB"
	// 					+ users.get(0).getUserName());
	// 		} else {
	// 			System.out.println("user doesnot exist");
	// 		}
	// 	} catch (NullPointerException e) {
	// 		e.getStackTrace();
	// 		throw e;
	// 	} catch (Exception e) {
	// 		e.getStackTrace();
	// 	}
	// 	finally {
	// 		if (factory != null) {
	// 			ctx.close();
	// 			manager.close();
	// 			factory.close();
	// 		}
	// 	}
	// 	return userFound;
	// }
	@SuppressWarnings("unchecked")
	public List<String> getRolesFromUserName(String userName){
		// TODO - change to type safe
		// this is also wrong because of string concat and security
		// at least =? shall be used so that he will take care of " and '
		// for sql injection crack
		// and this is an appscan error by the way
		Query query = manager
			.createQuery("Select role.roleName From Role role,UserRole userRole,User user" +
						 " where user.userName ='" +userName + "' "+
						 "and user = userRole.user and role = userRole.role");
		return query.getResultList();
		// Role roleFound = null;
		// CriteriaBuilder cb = manager.getCriteriaBuilder();

		
		// CriteriaQuery<Role> listCriteria = cb.createQuery(Role.class);
		// Root<Role> listRoot = listCriteria.from(Role.class);
		// listCriteria.select(listRoot);
		// //query.where(cb.equal(updateRoot.get(Role_.roleName),roleName));

		// // TODO
		// // this query now returns all roles
		// //listCriteria.where(cb.equal(listRoot.get(Role_.roleName),roleName));
		
		// TypedQuery<Role> tQuery = manager.createQuery(listCriteria);
		// return tQuery.getResultList();
	}

	// private Role getRoleFromName(String userName) {
	// 	// This is not type safe
	// 	// Role admin =
	// 	// manager.createQuery("Select a From Role a where Role.name=admin").getResultList();
	// 	// This mess below is type safe
	// 	Role roleFound = null;

	// 	try {
	// 		//TODO - change to type safe
	// 		Query tquery = manager
	// 				.createQuery("select role.role_name from user,user_role,role where user.user_name = "
	// 						+ userName
	// 						+ "and user.id = user_role.user_id and role.id = user_role.role_id");

	// 		/*
	// 		 * CriteriaBuilder cb = manager.getCriteriaBuilder();
	// 		 * CriteriaQuery<Role> listCriteria = cb.createQuery(Role.class);
	// 		 * Root<Role> listRoot = listCriteria.from(Role.class);
	// 		 * listCriteria.select(listRoot);
	// 		 * //query.where(cb.equal(updateRoot.get(Role_.roleName),roleName));
	// 		 * listCriteria
	// 		 * .where(cb.equal(listRoot.get(Role_.roleName),roleName));
	// 		 * TypedQuery<Role> tQuery = manager.createQuery(listCriteria);
	// 		 */

	// 		List<Role> roles = tquery.getResultList();
	// 		if (roles.size() == 1) {
	// 			roleFound = roles.get(0);
	// 		} else {
	// 			System.out.println("size:" + roles.size());
	// 			for (Role extractedRole : roles) {
	// 				System.out.println(extractedRole.getRoleName());
	// 			}
	// 		}
	// 	} catch (NullPointerException e) {
	// 		e.getStackTrace();
	// 		throw e;
	// 	} catch (Exception e) {
	// 		e.getStackTrace();
	// 	}
	// 	return roleFound;
	// }
}
