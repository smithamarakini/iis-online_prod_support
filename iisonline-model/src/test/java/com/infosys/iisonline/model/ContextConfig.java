package com.infosys.iisonline.model;

import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Spring Java based application context config
 */
@ComponentScan("com.infosys.iisonline.model")
@Configuration
public class ContextConfig {
	/**
	 * This bean is needed to initialize liquibase with spring test
	 */
	@Bean
	public SpringLiquibase springLiquibase(){
		final SpringLiquibase springLiquibase=new SpringLiquibase();
		springLiquibase.setDataSource(iisOnlineDataSource());
		//springLiquibase.setDefaultSchema(defaultSchema);
		springLiquibase.setChangeLog("classpath:liquibase/development.xml");
		return springLiquibase;
	}
	/**
	 * DataSource configured locally here
	 */
	@Bean
	public DataSource iisOnlineDataSource() {
		// TODO maybe to be centralized across maven projects
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.hsqldb.jdbc.JDBCDriver");
		// this is hsqldb in memory
		dataSource.setUrl("jdbc:hsqldb:mem:sandboxDb");
		dataSource.setUsername("SA");
		dataSource.setPassword("");
		return dataSource;
	}
}
