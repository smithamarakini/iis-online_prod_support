package com.infosys.iisonline.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Expression;
import javax.persistence.metamodel.EntityType;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;

import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.ContextConfiguration;
import javax.validation.ConstraintViolationException;

/**
 * This is the one and only test of the model package to check if the JPA integration setup works fine
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ContextConfig.class)
public class JpaIntgTest {
	private EntityManager manager = null;
	private EntityManagerFactory factory = null;
	private AnnotationConfigApplicationContext ctx = null;
	
	/**
	 * Initialize entityManager
	 */
	@Before
	public void before() {
		factory = Persistence.createEntityManagerFactory("persistenceUnit");
		manager = factory.createEntityManager();
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(ContextConfig.class);
		ctx.refresh();
	}
	/**
	 * Close  factory and entity Manager
	 */
	@After
	public void after() {
		// close spring
		ctx.close();
		// TODO Eventually clean liquibase
		manager.close();
		factory.close();
	}

	/**
	 * Blueprint Example for JPA tx
	 */
	@Test
	public void testRawJpaTransaction() {

		EntityTransaction tx = manager.getTransaction();

		tx.begin();
		try {
			createProviders();
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		listProviders(5);
		System.out.println(".. done");
	}

	/**
	 * Sample Test for checking the security.xml
	 */
	@Test
	public void testSecurityDB() {

		EntityTransaction tx = manager.getTransaction();

		tx.begin();
		try {
			createUsers();
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		listUsers(5);
		System.out.println(".. done");
	}
	/**
	 * Test for checking user name is a Email
	 */
	@Test
	public void testUserNameIsNotValidEmail() {

		EntityTransaction tx = manager.getTransaction();
		boolean validationSuccess = true;
		tx.begin();
		try {
			Role user = getRoleFromName("user");
			User captainAmerica = new User("@captain.com", "101",true);
			manager.persist(captainAmerica);
			UserRole captainAmericaUser = new UserRole(captainAmerica,user);
			manager.persist(captainAmericaUser);
		} catch (ConstraintViolationException e) {
			validationSuccess = false;
		}
		if(validationSuccess){
			fail();
		}
		//tx.commit();
	}

	private Role getRoleFromName(String roleName){
		// This is not type safe
		// Role admin = manager.createQuery("Select a From Role a where Role.name=admin").getResultList();
		// This mess below is type safe
		Role roleFound = null;
		CriteriaBuilder cb = manager.getCriteriaBuilder();
		
		CriteriaQuery<Role> listCriteria = cb.createQuery(Role.class);
		Root<Role> listRoot = listCriteria.from(Role.class);
		listCriteria.select(listRoot);
		//query.where(cb.equal(updateRoot.get(Role_.roleName),roleName));
		listCriteria.where(cb.equal(listRoot.get(Role_.roleName),roleName));
		
		TypedQuery<Role> tQuery = manager.createQuery(listCriteria);
		List<Role> roles = tQuery.getResultList();
 		if( roles.size() == 1){
			roleFound = roles.get(0); 
		}else{
			System.out.println("size:" + roles.size());
			for(Role extractedRole : roles){
				System.out.println(extractedRole.getRoleName());
			}
			fail("Expected one and only one <" + roleName + ">  role");
		}
		return roleFound;
	}
	private void createUsers() {
		Role admin = getRoleFromName("admin");
		Role user = getRoleFromName("user");
		//User sabin = new User("Sabin@sambin.com", "101",true);
		//User nemo = new User("CaptainNemo@trusteagle.com", "9002",false);
		User sabin = new User("Sabin@sambin.nl", "101",true);
		User nemo = new User("CaptainNemo@trusteagle.com", "9002",false);
		manager.persist(sabin);
		manager.persist(nemo);
		UserRole sabinAdmin = new UserRole(sabin,admin);
		manager.persist(sabinAdmin);
		UserRole nemoUser = new UserRole(nemo,user);
		manager.persist(nemoUser);
	}

	private void createProviders() {
		manager.persist(new Provider("JakabGipsz", "9001", "/callMe"));
		manager.persist(new Provider("CaptainNemo", "9002", "/callYou"));
	}
	@SuppressWarnings("unchecked")
	private void listUsers(int expectedNumUsers) {
		// TODO find a better method
		List<User> resultList = manager.createQuery("Select a From User a").getResultList();
		assertEquals(expectedNumUsers,resultList.size());
	}
	// TODO check if there is a better way
	@SuppressWarnings("unchecked")
	private void listProviders(int expectedNumProviders) {
		// TODO find a better method
		List<Provider> resultList = manager.createQuery("Select a From Provider a").getResultList();
		assertEquals(expectedNumProviders,resultList.size());
		System.out.println("num:" + resultList.size());
		for (Provider next : resultList) {
			System.out.println("name: " + next.getServerName() +
						" port: " + next.getServerPort());
		}
		assertThat(resultList,
		        IsIterableContainingInAnyOrder.<Provider> containsInAnyOrder(
		        		hasProperty("serverName", is("CaptainNemo")),
		        		hasProperty("serverName", is("JakabGipsz")),
		        		hasProperty("serverName", is("provider1")),
		        		hasProperty("serverName", is("provider2")),
		        		hasProperty("serverName", is("provider3"))
		        	));
	}
}
