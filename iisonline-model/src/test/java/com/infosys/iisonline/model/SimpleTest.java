package com.infosys.iisonline.model;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * simple Test to check if this comes first (mvn test) with respect to
 * Intg Tests (mvn integration-test)
 */
public class SimpleTest {
	@Before
	public void before() {
		System.out.println("Before");
	}
	/**
	 * Close  factory and entity Manager
	 */
	@After
	public void after() {
		System.out.println("After");
	}

	@Test
	public void testSimple() {
		System.out.println("Test");		
		assertEquals("Test:",true,true);
	}
}
