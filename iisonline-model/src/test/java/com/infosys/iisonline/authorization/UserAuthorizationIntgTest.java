package com.infosys.iisonline.authorization;

import java.util.List;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.ContextConfiguration;
import com.infosys.iisonline.model.ContextConfig;
import com.infosys.iisonline.model.Role;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ContextConfig.class)
public class UserAuthorizationIntgTest {
	private EntityManager manager = null;
	private EntityManagerFactory factory = null;
	private AnnotationConfigApplicationContext ctx = null;
	
	/**
	 * Initialize entityManager
	 */
	@Before
	public void before() {
		factory = Persistence.createEntityManagerFactory("persistenceUnit");
		manager = factory.createEntityManager();
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(ContextConfig.class);
		ctx.refresh();
	}
	/**
	 * Close  factory and entity Manager
	 */
	@After
	public void after() {
		// close spring
		ctx.close();
		// TODO Eventually clean liquibase
		manager.close();
		factory.close();
	}
	/**
	 * testing join query for getting roles for users
	 */
	@Test
	public void listRolesForUser(){
		UserAuthorization userAuthorization = new UserAuthorization();
		List<String> userRoles = userAuthorization.getRolesFromUserName("Sandy");
		for(String role :  userRoles){
			System.out.println("Fetched Role : <" + role +">");// role.getRoleName());
		}
		// sandy has admin and user
		assertEquals(2,userRoles.size());
		// sabin has "user" only
		List<String> sabinRoles = userAuthorization.getRolesFromUserName("Sabin");
		assertEquals(sabinRoles.get(0),"user");		
	}

}
