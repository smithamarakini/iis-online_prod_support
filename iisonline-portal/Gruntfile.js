module.exports = function(grunt) {

// Project configuration.
grunt.initConfig({
 pkg: grunt.file.readJSON('package.json'),
 
   clean: {
      dist: ['target\iisonline-portal\static\css', 'target\iisonline-portal\static\html','target\iisonline-portal\static\js'],	
      docs: 'target\iisonline-portal\static\docs'
    },
	
	compress: {
      main: {
        options: {
          archive: 'bootstrap-<%= pkg.version %>-dist.zip',
          mode: 'zip',
          level: 9,
          pretty: true
        },
        files: [
          {
            expand: true,
            cwd: 'dist/',
            src: ['**'],
            dest: 'bootstrap-<%= pkg.version %>-dist'
          }
        ]
      }
    },
	
	concat: {
      options: {
        banner: '<%= banner %>\n<%= jqueryCheck %>\n<%= jqueryVersionCheck %>',
        stripBanners: false
      },
      bootstrap: {
        src: [
          'src/main/webapp/static/js/bootstrap.js','src/main/webapp/static/js/iisonline.js'
          ],
        dest: 'target/iisonline-portal/static/js/build.js'
      }
    },

    uglify: {
      options: {
        compress: {
          warnings: false
        },
        mangle: true,
        preserveComments: 'some'
      },
      core: {
        src: '<%= concat.bootstrap.dest %>',
        dest: 'target/iisonline-portal/static/js/<%= pkg.name %>.min.js'
      },
      docsJs: {
        src: 'target/iisonline-portal/static/js/*.min.js',
        dest: 'target/iisonline-portal/static/docs/docs.min.js'
      }
    },
	
	cssmin: {
      options: {
        // TODO: disable `zeroUnits` optimization once clean-css 3.2 is released
        //    and then simplify the fix for https://github.com/twbs/bootstrap/issues/14837 accordingly
        compatibility: 'ie8',
        keepSpecialComments: '*',
        advanced: false
      },
      minifyCore: {
		files:[
        {src: ['src/main/webapp/static/css/bootstrap.css'],dest: 'target/iisonline-portal/static/css/bootstrap.min.css'},
		 {src: ['src/main/webapp/static/css/iisonline.css'],dest: 'target/iisonline-portal/static/css/iisonline.min.css'}
  		],
      },
      
	  
	  
      docs: {
        src: [
          'src/main/webapp/static/css/bootstrap.css',
          'src/main/webapp/static/css/iisonline.css'
        ],
        dest: 'target/iisonline-portal/static/docs/docs.min.css'
      }
    },
	
	htmlmin: {
        options: {
            removeComments: true
        },
        file: {
            options: {
                collapseWhitespace: true
            },
            files: {
                src: 'src/main/webapp/static/html/login.html',
                dest: 'target/iisonline-portal/static/html/'
            }
        }
    },
	jsdoc : {
        dist : {
            src: ['src/main/webapp/static/css/*.css'],
            options: {
                destination: 'target/iisonline-portal/static/docs/'
            }
        }
    }
	
  });

  // These plugins provide necessary tasks.
  //require('load-grunt-tasks')(grunt, { scope: 'devDependencies' });
  grunt.loadNpmTasks('grunt-contrib-clean'); 
  grunt.loadNpmTasks('grunt-contrib-concat'); 
  grunt.loadNpmTasks('grunt-contrib-cssmin'); 
  grunt.loadNpmTasks('grunt-contrib-htmlmin'); 
  grunt.loadNpmTasks('grunt-contrib-uglify'); 
  grunt.loadNpmTasks('grunt-contrib-watch'); 
  grunt.loadNpmTasks('grunt-html'); 
  grunt.loadNpmTasks('grunt-jsdoc');
  //grunt.loadNpmTasks('npm-shrinkwrap');
  //grunt.loadNpmTasks('load-grunt-tasks'); 
 // grunt.loadNpmTasks('time-grunt'); 
  
  
  require('time-grunt')(grunt);

  // JS distribution task.
  grunt.registerTask('dist-js', ['concat', 'uglify:core']);

  // CSS distribution task.
  grunt.registerTask('dist-css', ['cssmin:minifyCore']);
 
  // Full distribution task.
  grunt.registerTask('dist', ['clean:dist','dist-css','dist-js']);

  // Default task.
 // grunt.registerTask('default', ['clean:dist','test']);
    grunt.registerTask('default', ['clean:dist']);
  
  // Docs task.
  grunt.registerTask('docs-css', ['cssmin:docs']);
  grunt.registerTask('dist-js', ['uglify:docsJs']);
  grunt.registerTask('dist-docs', ['jsdoc:dist']);
  grunt.registerTask('docs', ['clean:docs','docs-css', 'dist-js','dist-docs']);

  
  grunt.registerTask('prep-release', ['dist', 'docs', 'htmlmin']);
};




