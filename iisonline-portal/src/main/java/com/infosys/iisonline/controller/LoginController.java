package com.infosys.iisonline.controller;

import java.util.List;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import com.infosys.iisonline.authorization.UserAuthorization;

@Controller
@RequestMapping("/")
public class LoginController {
	private UserAuthorization userAuthorization = null;
	
	public LoginController(){
		userAuthorization = new UserAuthorization();
	}
	@RequestMapping(value = { "/" ,"/welcome**"}, method = RequestMethod.GET)
	public ModelAndView defaultPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security Login Form - Database Authentication");
		model.addObject("message", "This is default page!");
		// /WEB-INF/pages/hello.jsp
		model.setViewName("/static/html/Investors_Landing_Page.html");
		return model;

	}
	@RequestMapping(value = {"/user**" }, method = RequestMethod.GET)
	public ModelAndView userPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security Login Form - Database Authentication");
		model.addObject("message", "This is user page!");
		model.setViewName("/static/sandbox/Dashboard/dashboard.html");
		//model.setViewName("/WEB-INF/pages/user.jsp");
		return model;
	}

	@RequestMapping(value = "/admin**", method = RequestMethod.GET)
	public ModelAndView adminPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security Login Form - Database Authentication");
		model.addObject("message", "This page is for ROLE_ADMIN only!");
		model.setViewName("/WEB-INF/pages/admin.jsp");

		return model;

	}

	@RequestMapping(value = "/logon", method = RequestMethod.GET)
	public ModelAndView logon(@RequestParam(value = "error", required = false) String error,
							  @RequestParam(value = "logout", required = false) String logout
							  ) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("/WEB-INF/pages/login.jsp");

		return model;

	}
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
							  @RequestParam(value = "logout", required = false) String logout
							  ) {
		ModelAndView model = new ModelAndView();

		model.setViewName("/static/html/login/inlogin.html");
		return model;
	}
	@RequestMapping(value = "/auth", method = RequestMethod.GET)
	public ModelAndView authLogin(HttpServletRequest request){
		//@ModelAttribute UserAuthorization userAuthorization ) {

		ModelAndView model = new ModelAndView();

		String userName = request.getUserPrincipal().getName();
			//Parameter("user");
		
		List<String> userRoles = userAuthorization.getRolesFromUserName(userName);
		for(String roleName :  userRoles){
		 	// TODO this is kinda unsafe double check
		 	// and it imply that user shall not have role guest
		 	if("admin".equalsIgnoreCase(roleName)){
				model.addObject("title", "Spring Security Login Form - Database Authentication");
				model.addObject("message", "This page is for ROLE_ADMIN only!");
		 		model.setViewName("/WEB-INF/pages/admin.jsp");
		 		break;
		 	}else if("user".equalsIgnoreCase(roleName)){
		 		model.setViewName("/static/sandbox/Dashboard/dashboard.html");	
		 	}else{
		 		model.setViewName("/static/html/Investors_Landing_Page.html");
		 	}
		 }
		return model;
	}
	
	//for 403 access denied page
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();
		
		//check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			System.out.println(userDetail);
		
			model.addObject("username", userDetail.getUsername());
			
		}
		
		model.setViewName("/WEB-INF/pages/403.jsp");
		return model;

	}

	/*
@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public ModelAndView adminPage() {

		ModelAndView model = new ModelAndView();
		//model.addObject("title", "Spring Security Login Form - Database Authentication");
		//model.addObject("message", "This page is for ROLE_ADMIN only!");
		model.setViewName("admin");

		return model;

	}

	*/
	/*public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {
	}*/
	
	/*
	  @RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(ModelMap map) {

		ModelAndView model = new ModelAndView();
	*/
	/*if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}*/
	/*	model.setViewName("login");
		map.addAttribute("message", "You've successfully logged in");

		return model;

	}
	
	//for 403 access denied page
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();
		
		//check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			System.out.println(userDetail);
		
			model.addObject("username", userDetail.getUsername());
			
		}
		
		model.setViewName("403");
		return model;

	}
	*/
}
