package com.backbase.portal.foundation.integration.persistence.hibernate.cache;

import org.hibernate.SessionFactory;
import org.hibernate.ejb.HibernateEntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.EntityManagerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.hibernate.stat.Statistics;

public class HibernateStatisticsFactoryBean implements FactoryBean<Statistics> {
	
	@Autowired
	private SessionFactory sessionFactory;
	//private EntityManagerFactory entityManagerFactory;

	@Override
	public Statistics getObject() throws Exception {
		//SessionFactory sessionFactory = ((HibernateEntityManagerFactory) entityManagerFactory)
		//		.getSessionFactory();
		return sessionFactory.getStatistics();
	}

	@Override
	public Class<?> getObjectType() {
		return Statistics.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}
}
