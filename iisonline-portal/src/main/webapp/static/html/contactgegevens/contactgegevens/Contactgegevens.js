$(document).ready(function() {  
         $.validator.addMethod("textOnly",  
               function(value, element)  
               {  
                 return this.optional(element) || /^[a-z]+$/i.test(value);  
                },  
                "Characters Only."  
         );  
         $("#register-form").validate(  
         {  
           rules:  
             { 
			 radio:{required:true},
			 surname:{required:true,textOnly:true},
			 year:{required:true,number:true},
			 Address:{required:true,minlength:5,maxlength:10,placeholder:true},  
             Postcode:{required:true,number:true,minlength:8,maxlength:10,placeholder:true},  
             Residence:{required:true,placeholder:true},
			 txtemail:{required:true,email:true},
			 txtcnct:{required:true,number:true,minlength:10,maxlength:10},
			 bankAccount:{required:true} 
           }, 
			
			messages:{  
			 surname:{textOnly:"Only alphabets allowed"},   
             Address:{minlength:"write complete address",maxlength:"address doesnot contain special character "},  
             Postcode:{number:"Please enter only numberic value ",minlength:"postcode must be of 3-6 characters",maxlength:"postcode must be of 3-6 characters"},  
             txtcnct:{number:"Please enter only numberic value ",minlength:"Contact no. must be of 10 digits",maxlength:"Contact no. must be of 10 digits"},  
             txtemail:{email:"Enter valid email"}
           }, 
            
           
           submitHandler: function(form) {  
             form.submit();  
           }  
         });  
       }); $(document).ready(function() {  
         $.validator.addMethod("textOnly",  
               function(value, element)  
               {  
                 return this.optional(element) || /^[a-z]+$/i.test(value);  
                },  
                "Characters Only."  
         );  
         $("#register-form").validate(  
         {  
           rules:  
             { 
			 radio:{required:true},
			 surname:{required:true,textOnly:true},
			 year:{required:true,number:true},
			 Address:{required:true,minlength:5,maxlength:10,placeholder:true},  
             Postcode:{required:true,number:true,minlength:8,maxlength:10,placeholder:true},  
             Residence:{required:true,placeholder:true},
			 txtemail:{required:true,email:true},
			 txtcnct:{required:true,number:true,minlength:10,maxlength:10},
			 bankAccount:{required:true} 
           }, 
			
			messages:{  
			 surname:{textOnly:"Only alphabets allowed"},   
             Address:{minlength:"write complete address",maxlength:"address doesnot contain special character "},  
             Postcode:{number:"Please enter only numberic value ",minlength:"postcode must be of 3-6 characters",maxlength:"postcode must be of 3-6 characters"},  
             txtcnct:{number:"Please enter only numberic value ",minlength:"Contact no. must be of 10 digits",maxlength:"Contact no. must be of 10 digits"},  
             txtemail:{email:"Enter valid email"}
           }, 
            
           
           submitHandler: function(form) {  
             form.submit();  
           }  
         });  
       }); $(document).ready(function() {  
         $.validator.addMethod("textOnly",  
               function(value, element)  
               {  
                 return this.optional(element) || /^[a-z]+$/i.test(value);  
                },  
                "Characters Only."  
         );  
         $("#register-form").validate(  
         {  
           rules:  
             { 
			 radio:{required:true},
			 surname:{required:true,textOnly:true},
			 year:{required:true,number:true},
			 Address:{required:true,minlength:5,maxlength:10,placeholder:true},  
             Postcode:{required:true,number:true,minlength:8,maxlength:10,placeholder:true},  
             Residence:{required:true,placeholder:true},
			 txtemail:{required:true,email:true},
			 txtcnct:{required:true,number:true,minlength:10,maxlength:10},
			 bankAccount:{required:true} 
           }, 
			
			messages:{  
			 surname:{textOnly:"Only alphabets allowed"},   
             Address:{minlength:"write complete address",maxlength:"address doesnot contain special character "},  
             Postcode:{number:"Please enter only numberic value ",minlength:"postcode must be of 3-6 characters",maxlength:"postcode must be of 3-6 characters"},  
             txtcnct:{number:"Please enter only numberic value ",minlength:"Contact no. must be of 10 digits",maxlength:"Contact no. must be of 10 digits"},  
             txtemail:{email:"Enter valid email"}
           }, 
            
           
           submitHandler: function(form) {  
             form.submit();  
           }  
         });  
       }); 